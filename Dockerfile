FROM openjdk:17-alpine
EXPOSE 8080
COPY target/*.jar /home/itau-pix.jar
ENTRYPOINT ["java","-jar","/home/itau-pix.jar"]
