package br.com.itau.itaupix.util;

public class CpfCnpjUtils {
  private static final int[] weightCPF = {11, 10, 9, 8, 7, 6, 5, 4, 3, 2};
  private static final int[] weightCNPJ = {6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2};

  private static int calculateDigit(String str, int[] weight) {
    int sum = 0;
    for (int index=str.length()-1, digit; index >= 0; index-- ) {
      digit = Integer.parseInt(str.substring(index,index+1));
      sum += digit*weight[weight.length-str.length()+index];
    }
    sum = 11 - sum % 11;
    return sum > 9 ? 0 : sum;
  }

  public static boolean isValidCPF(String cpf) {
    if ((cpf==null) || (cpf.length()!=11)) return false;

    int digit1 = calculateDigit(cpf.substring(0,9), weightCPF);
    int digit2 = calculateDigit(cpf.substring(0,9) + digit1, weightCPF);
    return cpf.equals(cpf.substring(0,9) + digit1 + digit2);
  }

  public static boolean isValidCNPJ(String cnpj) {
    if ((cnpj==null)||(cnpj.length()!=14)) return false;

    int digit1 = calculateDigit(cnpj.substring(0,12), weightCNPJ);
    int digit2 = calculateDigit(cnpj.substring(0,12) + digit1, weightCNPJ);
    return cnpj.equals(cnpj.substring(0,12) + digit1 + digit2);
  }
}
