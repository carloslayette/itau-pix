package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.model.TypeKey;
import org.springframework.stereotype.Component;

@Component
public class BusinessValidationFactory {

  public BusinessValidation createBusinessValidation(TypeKey typeKey) {

    BusinessValidation businessValidation = null;

    switch (typeKey) {
      case CPF -> businessValidation = new CPFValidation();
      case CNPJ -> businessValidation = new CNPJValidation();
      case EMAIL -> businessValidation = new EmailValidation();
    }
    return businessValidation;
  }
}
