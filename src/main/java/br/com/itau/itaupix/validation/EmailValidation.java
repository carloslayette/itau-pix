package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.InvalidEmailException;
import br.com.itau.itaupix.model.PIXKey;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class EmailValidation implements BusinessValidation {

  private final String EMAIL_REGEX = "(?=.{3,77}$)^(.+)@(.+)$";

  @Override
  public void validate(PIXKey pixKey) {

    if (!Pattern.matches(EMAIL_REGEX, pixKey.getKeyValue())) {
      throw new InvalidEmailException(ErrorModel.builder()
          .error("type key EMAIL does not match the expected pattern")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }
}
