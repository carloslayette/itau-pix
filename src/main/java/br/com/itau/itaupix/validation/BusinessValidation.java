package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.model.PIXKey;

public interface BusinessValidation {

  void validate(PIXKey pixKey);

}
