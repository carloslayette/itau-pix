package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.InvalidCPFException;
import br.com.itau.itaupix.model.PIXKey;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

import static br.com.itau.itaupix.util.CpfCnpjUtils.isValidCPF;

@Component
public class CPFValidation implements BusinessValidation {

  private final String CPF_REGEX = "[0-9]{11}";

  @Override
  public void validate(PIXKey pixKey) {
    if (!Pattern.matches(CPF_REGEX, pixKey.getKeyValue())) {
      throw new InvalidCPFException(ErrorModel
          .builder()
          .error("type key CPF does not match the expected pattern")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }

    if (!isValidCPF(pixKey.getKeyValue())) {
      throw new InvalidCPFException(ErrorModel
          .builder()
          .error("The entered CPF is not valid")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }
}
