package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.model.PIXKey;
import org.springframework.stereotype.Component;

@Component
public class UpdatePIXKeyValidation extends PIXKeyValidation implements BusinessValidation {

  @Override
  public void validate(PIXKey pixKey) {

    validateTypeAccount(pixKey);
    validateTypeEntity(pixKey);
  }
}
