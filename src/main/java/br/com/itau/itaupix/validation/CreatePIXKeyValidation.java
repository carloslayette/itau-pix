package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.exception.NumberOfPIXKeysExceededException;
import br.com.itau.itaupix.exception.PIXKeyAlreadyExists;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeEntity;
import br.com.itau.itaupix.model.TypeKey;
import br.com.itau.itaupix.repository.PIXKeyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreatePIXKeyValidation extends PIXKeyValidation implements BusinessValidation {

  private final Integer LIMIT_PIX_KEYS_ENTITY_F = 5;
  private final Integer LIMIT_PIX_KEYS_ENTITY_J = 20;

  private PIXKeyRepository pixKeyRepository;
  private BusinessValidationFactory businessValidationFactory;

  @Autowired
  public CreatePIXKeyValidation(PIXKeyRepository pixKeyRepository,
                                BusinessValidationFactory businessValidationFactory) {
    this.pixKeyRepository = pixKeyRepository;
    this.businessValidationFactory = businessValidationFactory;
  }

  @Override
  public void validate(PIXKey pixKey) throws BusinessValidationFailedException {

    validateTypeKey(pixKey);
    super.validateTypeAccount(pixKey);
    super.validateTypeEntity(pixKey);
    validateIfTypeKeyAlreadyExists(pixKey);
    validateIfAccountHasMoreKeysThanAllowed(pixKey);
  }

  private void validateTypeKey(PIXKey pixKey) {
    TypeKey typeKey = TypeKey.fetchValue(pixKey.getTypeKey());

    if (typeKey == null) {
      throw new BusinessValidationFailedException(ErrorModel.builder()
          .error("typeKey is not supported")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
    businessValidationFactory.createBusinessValidation(typeKey).validate(pixKey);
  }

  private void validateIfAccountHasMoreKeysThanAllowed(PIXKey pixKey) {

    Integer numberOfKeys = pixKeyRepository.countByAccountAndTypeEntity(pixKey.getBankBranchNumber(),
        pixKey.getAccountNumber(), pixKey.getTypeEntity());

    if (pixKey.getTypeEntity().equals(TypeEntity.FISICA.getValue()) && numberOfKeys >= LIMIT_PIX_KEYS_ENTITY_F) {
      throw new NumberOfPIXKeysExceededException(ErrorModel
          .builder()
          .error("Number of pixKeys per account exceeded")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }

    if (pixKey.getTypeEntity().equals(TypeEntity.JURIDICA.getValue()) && numberOfKeys >= LIMIT_PIX_KEYS_ENTITY_J) {
      throw new NumberOfPIXKeysExceededException(ErrorModel
          .builder()
          .error("Number of pixKeys per account exceeded")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  private void validateIfTypeKeyAlreadyExists(PIXKey pixKey) {

    PIXKey pixKeyResponse = pixKeyRepository
        .existsPIXKeyByKeyValue(pixKey.getKeyValue());

    if (pixKeyResponse != null) {
      throw new PIXKeyAlreadyExists(ErrorModel
          .builder()
          .error("PIXKey with the same keyValue already exists")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }
}
