package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.InvalidCNPJException;
import br.com.itau.itaupix.model.PIXKey;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

import static br.com.itau.itaupix.util.CpfCnpjUtils.isValidCNPJ;

@Component
public class CNPJValidation implements BusinessValidation {

  private final String CNPJ_REGEX = "[0-9]{14}";

  @Override
  public void validate(PIXKey pixKey) {
    if (!Pattern.matches(CNPJ_REGEX, pixKey.getKeyValue())) {
      throw new InvalidCNPJException(ErrorModel
          .builder()
          .error("type key CNPJ does not match the expected pattern")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }

    if (!isValidCNPJ(pixKey.getKeyValue())) {
      throw new InvalidCNPJException(ErrorModel
          .builder()
          .error("The entered CNPJ is not valid")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }
}
