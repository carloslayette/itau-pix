package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeAccount;
import br.com.itau.itaupix.model.TypeEntity;

public abstract class PIXKeyValidation {

  public void validateTypeAccount(PIXKey pixKey) {

    TypeAccount typeAccount = TypeAccount.fetchValue(pixKey.getTypeAccount());

    if (typeAccount == null) {
      throw new BusinessValidationFailedException(ErrorModel.builder()
          .error("typeAccount is not supported")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  public void validateTypeEntity(PIXKey pixKey) {
    TypeEntity typeEntity = TypeEntity.fetchValue(pixKey.getTypeEntity());

    if (typeEntity == null) {
      throw new BusinessValidationFailedException(ErrorModel.builder()
          .error("typeEntity is not supported")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }
}
