package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import java.util.Objects;
import java.util.stream.Stream;
import org.springframework.stereotype.Component;

@Component
public class SearchPIXKeyValidation implements BusinessValidation {

  @Override
  public void validate(PIXKey pixKey) {

    validateAllFieldsAreNull(pixKey);
    validateIfAllOtherFieldAreNullExceptId(pixKey);
    validateIfBankBranchAndAccountNumberAreIncludedSeparated(pixKey);
    validateTypeKeyIsTheOnlyFilter(pixKey);
  }

  private void validateAllFieldsAreNull(PIXKey pixKey) {
    if (checkIfAllFieldsAreNull(pixKey)) {
      throw new BusinessValidationFailedException(ErrorModel
          .builder()
          .error("At least one field needs to be informed")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  private void validateIfAllOtherFieldAreNullExceptId(PIXKey pixKey) {
    if (pixKey.getId() != null && !checkIfAllOthersFieldsAreNullExceptId(pixKey)) {
      throw new BusinessValidationFailedException(ErrorModel
          .builder()
          .error("If id field is informed, the others fields mustn't be included")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  private void validateIfBankBranchAndAccountNumberAreIncludedSeparated(PIXKey pixKey) {
    if (pixKey.getId() == null && checkIfBankBranchAndAccountNumberAreIncludedSeparated(pixKey)) {
      throw new BusinessValidationFailedException(ErrorModel
          .builder()
          .error("If bankBranchNumber is informed, accountNumber needs to be informed as well and vice versa")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  private void validateTypeKeyIsTheOnlyFilter (PIXKey pixKey) {
    if (checkIfTypeKeyIsTheOnlyFilter(pixKey)) {
      throw new BusinessValidationFailedException(ErrorModel
          .builder()
          .error("The filter must include other fields such as bankBranchNumber and accountNumber")
          .path("/itau-pix/api/v1/pix-key")
          .build());
    }
  }

  private boolean checkIfAllFieldsAreNull(PIXKey pixKey) {
    return Stream.of(pixKey.getId(), pixKey.getTypeKey(),
        pixKey.getBankBranchNumber(), pixKey.getAccountNumber())
        .allMatch(Objects::isNull);
  }

  private boolean checkIfAllOthersFieldsAreNullExceptId(PIXKey pixKey) {
    return Stream.of(pixKey.getTypeKey(),
        pixKey.getBankBranchNumber(), pixKey.getAccountNumber())
        .allMatch(Objects::isNull);
  }

  private boolean checkIfTypeKeyIsTheOnlyFilter(PIXKey pixKey) {
    return pixKey.getTypeKey() != null && Stream.of(pixKey.getId(),
        pixKey.getBankBranchNumber(), pixKey.getAccountNumber())
        .allMatch(Objects::isNull);
  }

  private boolean checkIfBankBranchAndAccountNumberAreIncludedSeparated(PIXKey pixKey) {
    return (pixKey.getBankBranchNumber() != null && pixKey.getAccountNumber() == null) ||
        (pixKey.getBankBranchNumber() == null && pixKey.getAccountNumber() != null);
  }
}
