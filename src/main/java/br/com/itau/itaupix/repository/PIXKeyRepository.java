package br.com.itau.itaupix.repository;

import br.com.itau.itaupix.model.PIXKey;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PIXKeyRepository extends JpaRepository<PIXKey, UUID> {

  @Query("SELECT p FROM PIXKey p WHERE p.keyValue = ?1 and p.inactivationDate is null")
  PIXKey existsPIXKeyByKeyValue(String keyValue);

  @Query("SELECT COUNT(p) FROM PIXKey p WHERE p.bankBranchNumber = ?1 AND p.accountNumber = ?2 and p.typeEntity=?3 and p.inactivationDate" +
      " is null")
  Integer countByAccountAndTypeEntity(Integer bankBranchNumber, Long accountNumber, String typeEntity);

}
