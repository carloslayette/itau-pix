package br.com.itau.itaupix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItauPixApplication {

  public static void main(String[] args) {
    SpringApplication.run(ItauPixApplication.class, args);
  }

}
