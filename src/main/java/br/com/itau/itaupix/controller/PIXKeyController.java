package br.com.itau.itaupix.controller;


import br.com.itau.itaupix.dto.CreatePIXKeyRequest;
import br.com.itau.itaupix.dto.CreatePIXKeyResponse;
import br.com.itau.itaupix.dto.SearchPIXKeyRequest;
import br.com.itau.itaupix.dto.SearchPIXKeyResponse;
import br.com.itau.itaupix.dto.UpdatePIXKeyRequest;
import br.com.itau.itaupix.dto.UpdatePIXKeyResponse;
import br.com.itau.itaupix.service.IPIXKeyService;
import br.com.itau.itaupix.service.PIXKeyService;
import java.util.List;
import javax.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "api/v1/pix-key")
@Log4j2
public class PIXKeyController {

  private IPIXKeyService pixKeyService;

  @Autowired
  public PIXKeyController(PIXKeyService pixKeyService) {
    this.pixKeyService = pixKeyService;
  }

  @PostMapping
  public ResponseEntity<CreatePIXKeyResponse> add(@Valid @RequestBody final CreatePIXKeyRequest createPixKeyRequest) {

    log.info("Adding new PIXKey");
    CreatePIXKeyResponse createPixKeyResponse = pixKeyService.add(createPixKeyRequest);
    log.info("PIXKey added with id " + createPixKeyResponse.getId());
    return new ResponseEntity<>(createPixKeyResponse, HttpStatus.OK);
  }

  @PutMapping
  public ResponseEntity<UpdatePIXKeyResponse> update(@Valid @RequestBody final UpdatePIXKeyRequest updatePIXKeyRequest) {

    log.info("Updating new PIXKey");
    UpdatePIXKeyResponse updatePIXKeyResponse = pixKeyService.update(updatePIXKeyRequest);
    log.info("PIXKey updated with id " + updatePIXKeyResponse.getId());
    return new ResponseEntity<>(updatePIXKeyResponse, HttpStatus.OK);
  }

  @GetMapping
  public List<SearchPIXKeyResponse> search(@Param("id") String id,
                                           @Param("typeKey") String typeKey,
                                           @Param("bankBranchNumber") Integer bankBranchNumber,
                                           @Param("accountNumber") Long accountNumber) {

    return pixKeyService.search(SearchPIXKeyRequest
        .builder()
        .id(id)
        .typeKey(typeKey)
        .bankBranchNumber(bankBranchNumber)
        .accountNumber(accountNumber)
        .build());
  }
}
