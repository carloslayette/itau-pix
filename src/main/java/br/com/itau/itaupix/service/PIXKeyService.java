package br.com.itau.itaupix.service;

import br.com.itau.itaupix.dto.CreatePIXKeyRequest;
import br.com.itau.itaupix.dto.CreatePIXKeyResponse;
import br.com.itau.itaupix.dto.SearchPIXKeyRequest;
import br.com.itau.itaupix.dto.SearchPIXKeyResponse;
import br.com.itau.itaupix.dto.UpdatePIXKeyRequest;
import br.com.itau.itaupix.dto.UpdatePIXKeyResponse;
import br.com.itau.itaupix.exception.ErrorModel;
import br.com.itau.itaupix.exception.PIXKeyNotFoundException;
import br.com.itau.itaupix.mapper.PIXKeyMapper;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.repository.PIXKeyRepository;
import br.com.itau.itaupix.validation.CreatePIXKeyValidation;
import br.com.itau.itaupix.validation.SearchPIXKeyValidation;
import br.com.itau.itaupix.validation.UpdatePIXKeyValidation;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
public class PIXKeyService implements IPIXKeyService{

  private PIXKeyRepository pixKeyRepository;
  private CreatePIXKeyValidation createPixKeyValidation;
  private UpdatePIXKeyValidation updatePIXKeyValidation;
  private SearchPIXKeyValidation searchPIXKeyValidation;

  @Autowired
  public PIXKeyService(PIXKeyRepository pixKeyRepository,
                       CreatePIXKeyValidation createPixKeyValidation,
                       UpdatePIXKeyValidation updatePIXKeyValidation,
                       SearchPIXKeyValidation searchPIXKeyValidation) {

    this.pixKeyRepository = pixKeyRepository;
    this.createPixKeyValidation = createPixKeyValidation;
    this.updatePIXKeyValidation = updatePIXKeyValidation;
    this.searchPIXKeyValidation = searchPIXKeyValidation;

  }

  public PIXKeyService(PIXKeyRepository pixKeyRepository) {
    this.pixKeyRepository = pixKeyRepository;
  }

  @Transactional
  public CreatePIXKeyResponse add(CreatePIXKeyRequest createPixKeyRequest) {

    PIXKey pixKey = PIXKeyMapper.toPIXKey(createPixKeyRequest);

    createPixKeyValidation.validate(pixKey);

    pixKey.setCreationDate(LocalDateTime.now());
    PIXKey created = pixKeyRepository.save(pixKey);

    return CreatePIXKeyResponse.builder().id(created.getId()).build();
  }

  @Transactional
  public UpdatePIXKeyResponse update(UpdatePIXKeyRequest updatePIXKeyRequest) {

    PIXKey pixKey = pixKeyRepository.findById(updatePIXKeyRequest.getId())
        .orElseThrow(() -> new PIXKeyNotFoundException(ErrorModel
            .builder()
            .path("/itau-pix/api/v1/pix-key")
            .error("PIXKey with id " + updatePIXKeyRequest.getId() + " not found")
            .build()));

    pixKey.convertFromUpdatePIXKeyRequest(updatePIXKeyRequest);

    updatePIXKeyValidation.validate(pixKey);

    UpdatePIXKeyResponse updatePIXKeyResponse = PIXKeyMapper.toUpdatePIXKeyResponse(pixKeyRepository.save(pixKey));

    return updatePIXKeyResponse;
  }

  public List<SearchPIXKeyResponse> search(@Valid SearchPIXKeyRequest searchPIXKeyRequest) {

    PIXKey pixKey = PIXKeyMapper.toPIXKey(searchPIXKeyRequest);
    searchPIXKeyValidation.validate(pixKey);
    Example<PIXKey> filter = Example.of(pixKey);

    List<PIXKey> result = pixKeyRepository.findAll(filter);

    if (result.size() == 0) {
      throw new PIXKeyNotFoundException(ErrorModel
          .builder()
          .error("The filter has returned no results")
          .build());
    }

    return PIXKeyMapper.toSearchPIXKeyResponseList(result);
  }
}
