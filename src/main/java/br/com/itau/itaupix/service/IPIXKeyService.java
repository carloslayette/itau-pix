package br.com.itau.itaupix.service;

import br.com.itau.itaupix.dto.CreatePIXKeyRequest;
import br.com.itau.itaupix.dto.CreatePIXKeyResponse;
import br.com.itau.itaupix.dto.SearchPIXKeyRequest;
import br.com.itau.itaupix.dto.SearchPIXKeyResponse;
import br.com.itau.itaupix.dto.UpdatePIXKeyRequest;
import br.com.itau.itaupix.dto.UpdatePIXKeyResponse;
import java.util.List;
import javax.validation.Valid;

public interface IPIXKeyService {

  CreatePIXKeyResponse add(CreatePIXKeyRequest createPixKeyRequest);

  UpdatePIXKeyResponse update(UpdatePIXKeyRequest updatePIXKeyRequest);

  List<SearchPIXKeyResponse> search(@Valid SearchPIXKeyRequest searchPIXKeyRequest);

}
