package br.com.itau.itaupix.dto;

import java.util.UUID;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UpdatePIXKeyRequest {

  @NotNull(message = "id is required")
  private UUID id;

  @NotNull(message = "typeAccount is required")
  @NotEmpty(message = "typeAccount cannot be empty")
  @Size(max = 10, message = "typeAccount shouldn't have more than 10 characters")
  private String typeAccount;

  @NotNull(message = "bankBranchNumber is required")
  @Min(value = 1)
  @Max(value = 9999, message = "bankBranchNumber shouldn't have more than 4 characters")
  private Integer bankBranchNumber;

  @NotNull(message = "accountNumber is required")
  @Min(value = 1)
  @Max(value = 99999999, message = "typeAccount shouldn't have more than 8 characters")
  private Long accountNumber;

  @NotNull(message = "typeEntity is required")
  @NotEmpty(message = "typeEntity cannot be empty")
  @Size(max = 1, message = "typeEntity shouldn't have more than 1 characters")
  private String typeEntity;

  @NotNull(message = "accountHolderFirstName is required")
  @NotBlank(message = "accountHolderFirstName cannot be empty")
  @Size(max = 30, message = "accountHolderFirstName shouldn't have more than 30 characters")
  private String accountHolderFirstName;

  @Size(max = 45, message = "accountHolderFirstName shouldn't have more than 30 characters")
  private String accountHolderLastName;

}
