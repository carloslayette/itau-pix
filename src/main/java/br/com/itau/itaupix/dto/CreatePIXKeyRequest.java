package br.com.itau.itaupix.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class CreatePIXKeyRequest {

  @NotNull(message = "typeKey is required")
  @NotBlank(message = "typeKey cannot be empty")
  @Size(max = 9, message = "typeKey shouldn't have more than 9 characters")
  private String typeKey;

  @NotNull(message = "keyValue is required")
  @NotBlank(message = "keyValue cannot be empty")
  @Size(max = 77, message = "keyValue shouldn't have more than 77 characters")
  private String keyValue;

  @NotNull(message = "bankBranchNumber is required")
  @Min(value = 1)
  @Max(value = 9999, message = "bankBranchNumber shouldn't have more than 4 characters")
  private Integer bankBranchNumber;

  @NotNull(message = "accountNumber is required")
  @Min(value = 1)
  @Max(value = 99999999, message = "accountNumber shouldn't have more than 8 characters")
  private Long accountNumber;

  @NotNull(message = "accountHolderFirstName is required")
  @NotBlank(message = "accountHolderFirstName cannot be empty")
  @Size(max = 30, message = "accountHolderFirstName shouldn't have more than 30 characters")
  private String accountHolderFirstName;

  @Size(max = 45, message = "accountHolderLastName shouldn't have more than 45 characters")
  private String accountHolderLastName;

  @NotNull(message = "typeEntity is required")
  @NotBlank(message = "typeEntity cannot be empty")
  @Size(max = 1, message = "typeEntity shouldn't have more than 1 characters")
  private String typeEntity;

  @NotNull(message = "typeAccount is required")
  @NotBlank(message = "typeAccount cannot be empty")
  @Size(max = 10, message = "typeAccount shouldn't have more than 10 characters")
  private String typeAccount;

}
