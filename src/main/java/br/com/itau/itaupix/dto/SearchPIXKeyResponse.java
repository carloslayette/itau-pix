package br.com.itau.itaupix.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SearchPIXKeyResponse {

  private UUID id;
  private String typeKey;
  private String keyValue;
  private Integer bankBranchNumber;
  private Long accountNumber;
  private String accountHolderFirstName;
  private String accountHolderLastName;
  private String typeEntity;
  private String typeAccount;
  @JsonFormat(pattern = ("dd/MM/YYYY"))
  private LocalDateTime creationDate;
  @JsonFormat(pattern = ("dd/MM/YYYY"))
  private LocalDateTime inactivationDate;

}
