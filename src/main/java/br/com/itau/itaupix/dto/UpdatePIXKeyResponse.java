package br.com.itau.itaupix.dto;

import java.time.LocalDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UpdatePIXKeyResponse {

  private UUID id;
  private String typeKey;
  private String keyValue;
  private String typeAccount;
  private Integer bankBranchNumber;
  private Long accountNumber;
  private String accountHolderFirstName;
  private String accountHolderLastName;
  private String typeEntity;
  private LocalDateTime creationDate;

}
