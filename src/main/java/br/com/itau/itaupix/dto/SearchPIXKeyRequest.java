package br.com.itau.itaupix.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@Getter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SearchPIXKeyRequest {

  private String id;

  @Size(max = 9, message = "typeKey shouldn't have more than 9 characters")
  private String typeKey;

  @Min(value = 1)
  @Max(value = 9999, message = "bankBranchNumber shouldn't have more than 4 characters")
  private Integer bankBranchNumber;

  @Min(value = 1)
  @Max(value = 99999999, message = "accountNumber shouldn't have more than 8 characters")
  private Long accountNumber;

}
