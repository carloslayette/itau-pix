package br.com.itau.itaupix.mapper;

import br.com.itau.itaupix.dto.CreatePIXKeyRequest;
import br.com.itau.itaupix.dto.SearchPIXKeyRequest;
import br.com.itau.itaupix.dto.SearchPIXKeyResponse;
import br.com.itau.itaupix.dto.UpdatePIXKeyResponse;
import br.com.itau.itaupix.model.PIXKey;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PIXKeyMapper {

  public static PIXKey toPIXKey(SearchPIXKeyRequest searchPIXKeyRequest) {
    return PIXKey.builder()
        .id(searchPIXKeyRequest.getId() != null && !searchPIXKeyRequest.getId().trim().isEmpty()
            ? UUID.fromString(searchPIXKeyRequest.getId())
            : null)
        .typeKey(searchPIXKeyRequest.getTypeKey())
        .bankBranchNumber(searchPIXKeyRequest.getBankBranchNumber())
        .accountNumber(searchPIXKeyRequest.getAccountNumber())
        .build();
  }

  public static PIXKey toPIXKey(CreatePIXKeyRequest createPixKeyRequest) {
    return PIXKey.builder()
        .typeKey(createPixKeyRequest.getTypeKey())
        .keyValue(createPixKeyRequest.getKeyValue())
        .bankBranchNumber(createPixKeyRequest.getBankBranchNumber())
        .accountNumber(createPixKeyRequest.getAccountNumber())
        .accountHolderFirstName(createPixKeyRequest.getAccountHolderFirstName())
        .accountHolderLastName(createPixKeyRequest.getAccountHolderLastName())
        .typeEntity(createPixKeyRequest.getTypeEntity())
        .typeAccount(createPixKeyRequest.getTypeAccount())
        .build();
  }

  public static UpdatePIXKeyResponse toUpdatePIXKeyResponse(PIXKey pixKey) {
    return UpdatePIXKeyResponse.builder()
        .id(pixKey.getId())
        .typeKey(pixKey.getTypeKey())
        .keyValue(pixKey.getKeyValue())
        .typeAccount(pixKey.getTypeAccount())
        .bankBranchNumber(pixKey.getBankBranchNumber())
        .accountNumber(pixKey.getAccountNumber())
        .accountHolderFirstName(pixKey.getAccountHolderFirstName())
        .accountHolderLastName(pixKey.getAccountHolderLastName())
        .typeEntity(pixKey.getTypeEntity())
        .creationDate(pixKey.getCreationDate())
        .build();
  }

  public static List<SearchPIXKeyResponse> toSearchPIXKeyResponseList(List<PIXKey> pixKeyList) {
    List<SearchPIXKeyResponse> result = new ArrayList<>();
    pixKeyList.forEach(pixKey -> result.add(SearchPIXKeyResponse.builder()
        .id(pixKey.getId())
        .typeKey(pixKey.getTypeKey())
        .keyValue(pixKey.getKeyValue())
        .bankBranchNumber(pixKey.getBankBranchNumber())
        .accountNumber(pixKey.getAccountNumber())
        .accountHolderFirstName(pixKey.getAccountHolderFirstName())
        .accountHolderLastName(pixKey.getAccountHolderLastName() != null ? "" : pixKey.getAccountHolderLastName())
        .typeEntity(pixKey.getTypeEntity())
        .typeAccount(pixKey.getTypeAccount())
        .creationDate(pixKey.getCreationDate())
        .inactivationDate(pixKey.getInactivationDate())
        .build()));
    return result;
  }

}
