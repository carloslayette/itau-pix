package br.com.itau.itaupix.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class BusinessValidationFailedException extends RuntimeException {

  private ErrorModel errorModel;

  public BusinessValidationFailedException(ErrorModel errorModel) {
    super(errorModel.getError());
    this.errorModel = errorModel;
  }
}
