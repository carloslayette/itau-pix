package br.com.itau.itaupix.exception;

public class InvalidEmailException extends BusinessValidationFailedException {

  public InvalidEmailException(ErrorModel errorModel) {
    super(errorModel);
  }
}
