package br.com.itau.itaupix.exception;

public class NumberOfPIXKeysExceededException extends BusinessValidationFailedException {

  public NumberOfPIXKeysExceededException(ErrorModel errorModel) {
    super(errorModel);
  }
}
