package br.com.itau.itaupix.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Log4j2
public class RestExceptionHandler {

  @ExceptionHandler(value = {BusinessValidationFailedException.class})
  private ResponseEntity<ErrorModel> handleBusinessException(BusinessValidationFailedException ex) {
    log.error("BusinessException: " + ex.getErrorModel().getError(),
        ex.getErrorModel().getError());
    ex.getErrorModel().setTimestamp(LocalDateTime.now());
    ex.getErrorModel().setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
    return new ResponseEntity<>(ex.getErrorModel(), HttpStatus.UNPROCESSABLE_ENTITY);
  }

  @ExceptionHandler(value = {PIXKeyNotFoundException.class})
  private ResponseEntity<ErrorModel> handleBusinessException(PIXKeyNotFoundException ex) {
    log.error("PIXKeyNotFoundException: " + ex.getErrorModel().getError(),
        ex.getErrorModel().getError());
    ex.getErrorModel().setTimestamp(LocalDateTime.now());
    ex.getErrorModel().setStatus(HttpStatus.NOT_FOUND.value());
    return new ResponseEntity<>(ex.getErrorModel(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(value = {MethodArgumentNotValidException.class})
  @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
  @ResponseBody
  private List<ErrorModel> onMethodArgumentNotValidException(MethodArgumentNotValidException e) {
    List<ErrorModel> errorsList = new ArrayList<>();

    e.getBindingResult().getFieldErrors().forEach(err -> errorsList.add(
        ErrorModel
            .builder()
            .error(err.getDefaultMessage())
            .timestamp(LocalDateTime.now())
            .status(HttpStatus.UNPROCESSABLE_ENTITY.value())
            .build()));

    return errorsList;
  }

  @ExceptionHandler(value = {ConstraintViolationException.class})
  private ResponseEntity<ErrorModel> handleConstraintViolationException(ConstraintViolationException ex) {

    ErrorModel error = ErrorModel.builder()
        .timestamp(LocalDateTime.now())
        .error(ex.getMessage())
        .path("api/v1/pix-key")
        .status(HttpStatus.UNPROCESSABLE_ENTITY.value())
        .build();

    log.error("ConstraintViolationException: " + error.getError());

    return new ResponseEntity<>(error, HttpStatus.UNPROCESSABLE_ENTITY);
  }

}
