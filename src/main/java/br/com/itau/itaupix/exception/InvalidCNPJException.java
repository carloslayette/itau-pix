package br.com.itau.itaupix.exception;

public class InvalidCNPJException extends BusinessValidationFailedException {

  public InvalidCNPJException(ErrorModel errorModel) {
    super(errorModel);
  }
}
