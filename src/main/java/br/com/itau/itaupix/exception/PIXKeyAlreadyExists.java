package br.com.itau.itaupix.exception;

public class PIXKeyAlreadyExists extends BusinessValidationFailedException {

  public PIXKeyAlreadyExists(ErrorModel errorModel) {
    super(errorModel);
  }
}
