package br.com.itau.itaupix.exception;

public class PIXKeyNotFoundException extends BusinessValidationFailedException {

  public PIXKeyNotFoundException(ErrorModel errorModel) {
    super(errorModel);
  }
}
