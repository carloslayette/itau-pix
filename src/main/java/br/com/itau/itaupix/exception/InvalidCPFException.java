package br.com.itau.itaupix.exception;

public class InvalidCPFException extends BusinessValidationFailedException {

  public InvalidCPFException(ErrorModel errorModel) {
    super(errorModel);
  }
}
