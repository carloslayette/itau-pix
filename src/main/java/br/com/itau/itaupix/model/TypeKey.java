package br.com.itau.itaupix.model;

public enum TypeKey {
  CPF("cpf"),
  CNPJ("cnpj"),
  CELULAR("celular"),
  EMAIL("email"),
  ALEATORIO("aleatorio");

  private String value;

  TypeKey(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  public static TypeKey fetchValue(String constant) {

    for (TypeKey typeKey : TypeKey.values()) {
      if (typeKey.value.equals(constant)) {
        return typeKey;
      }
    }
    return null;
  }
}
