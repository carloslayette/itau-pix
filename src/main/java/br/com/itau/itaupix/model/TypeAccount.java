package br.com.itau.itaupix.model;

public enum TypeAccount {
  CORRENTE("corrente"),
  POUPANCA("poupança");

  private String value;

  TypeAccount(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }

  public static TypeAccount fetchValue(String constant) {

    for (TypeAccount typeAccount : TypeAccount.values()) {
      if (typeAccount.value.equals(constant)) {
        return typeAccount;
      }
    }
    return null;
  }
}
