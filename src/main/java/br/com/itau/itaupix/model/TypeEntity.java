package br.com.itau.itaupix.model;

public enum TypeEntity {
  FISICA("F"),
  JURIDICA("J");

  private String value;

  TypeEntity(String value) {
    this.value = value;
  }

  public String getValue() {
    return this.value;
  }

  public static TypeEntity fetchValue(String constant) {

    for (TypeEntity typeEntity : TypeEntity.values()) {
      if (typeEntity.value.equals(constant)) {
        return typeEntity;
      }
    }
    return null;
  }

}
