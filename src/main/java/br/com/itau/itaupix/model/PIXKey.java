package br.com.itau.itaupix.model;

import br.com.itau.itaupix.dto.UpdatePIXKeyRequest;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

@Entity
@Table
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PIXKey {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", insertable = false, updatable = false, nullable = false)
  @Type(type = "uuid-char")
  private UUID id;

  @Column(length = 9, nullable = false)
  private String typeKey;

  @Column(length = 77, nullable = false)
  private String keyValue;

  @Column(length = 4, nullable = false)
  private Integer bankBranchNumber;

  @Column(length = 8, nullable = false)
  private Long accountNumber;

  @Column(length = 30, nullable = false)
  private String accountHolderFirstName;

  @Column(length = 45)
  private String accountHolderLastName;

  @Column(length = 1, nullable = false)
  private String typeEntity;

  @Column(length = 10, nullable = false)
  private String typeAccount;

  @Column(nullable = false, columnDefinition = "TIMESTAMP")
  private LocalDateTime creationDate;

  @Column(columnDefinition = "TIMESTAMP")
  private LocalDateTime inactivationDate;

  public void convertFromUpdatePIXKeyRequest(UpdatePIXKeyRequest updatePixKeyRequest) {
    setTypeAccount(updatePixKeyRequest.getTypeAccount());
    setBankBranchNumber(updatePixKeyRequest.getBankBranchNumber());
    setAccountNumber(updatePixKeyRequest.getAccountNumber());
    setTypeEntity(updatePixKeyRequest.getTypeEntity());
    setAccountHolderFirstName(updatePixKeyRequest.getAccountHolderFirstName());
    setAccountHolderLastName(updatePixKeyRequest.getAccountHolderLastName());
  }

}
