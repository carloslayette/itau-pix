package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CNPJValidationTest {

  private CNPJValidation underTest;

  @BeforeEach
  void setUp() {
    underTest = new CNPJValidation();
  }

  @Test
  public void cnpjValidationLessDigitsShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("cnpj")
        .keyValue("8937019900015")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("type key CNPJ does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
  }

    @Test
    public void cnpjValidationMoreDigitsShouldFail() {

      PIXKey pixKey =  PIXKey.builder()
          .typeKey("cnpj")
          .keyValue("893701990001531")
          .build();

      BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
      assertTrue(exception.getMessage().contains("type key CNPJ does not match the expected pattern"));
      assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
    }
  @Test
  public void cnpjValidationNotValidShouldFail() {

    PIXKey pixKey =  PIXKey.builder()
        .typeKey("cnpj")
        .keyValue("11111111111111")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("The entered CNPJ is not valid"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
  }

  @Test
  public void cnpjValidationShouldSuccess() {

    PIXKey pixKey =  PIXKey.builder()
        .typeKey("cnpj")
        .keyValue("14922739000165")
        .build();

    underTest.validate(pixKey);
  }
}
