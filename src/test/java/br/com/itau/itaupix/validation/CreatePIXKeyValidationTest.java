package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeAccount;
import br.com.itau.itaupix.model.TypeEntity;
import br.com.itau.itaupix.model.TypeKey;
import br.com.itau.itaupix.repository.PIXKeyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class CreatePIXKeyValidationTest {

  private CreatePIXKeyValidation underTest;

  @Mock
  private PIXKeyRepository pixKeyRepository;

  @BeforeEach
  void setUp() {
    underTest = new CreatePIXKeyValidation(pixKeyRepository, new BusinessValidationFactory());
  }

  @Test
  void createPIXKeyValidationEntityFisicaNumberOfKeysExceededTestShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("46481154065")
        .bankBranchNumber(2123)
        .accountNumber(12345L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.FISICA.getValue())
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .build();

    when(pixKeyRepository.existsPIXKeyByKeyValue(pixKey.getKeyValue())).thenReturn(null);
    when(pixKeyRepository.countByAccountAndTypeEntity(pixKey.getBankBranchNumber(), pixKey.getAccountNumber(), pixKey.getTypeEntity())).thenReturn(5);

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("Number of pixKeys per account exceeded"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  void createPIXKeyValidationEntityJuridicaNumberOfKeysExceededTestShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("46481154065")
        .bankBranchNumber(2123)
        .accountNumber(12345L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.JURIDICA.getValue())
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .build();

    when(pixKeyRepository.existsPIXKeyByKeyValue(pixKey.getKeyValue())).thenReturn(null);
    when(pixKeyRepository.countByAccountAndTypeEntity(pixKey.getBankBranchNumber(), pixKey.getAccountNumber(), pixKey.getTypeEntity())).thenReturn(20);

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("Number of pixKeys per account exceeded"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  void createPIXKeyValidationExistentKeyValueShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("46481154065")
        .bankBranchNumber(2123)
        .accountNumber(12345L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.FISICA.getValue())
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .build();

    PIXKey existent = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("46481154065")
        .bankBranchNumber(2123)
        .accountNumber(12345L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.FISICA.getValue())
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .build();

    when(pixKeyRepository.existsPIXKeyByKeyValue(pixKey.getKeyValue())).thenReturn(existent);

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("PIXKey with the same keyValue already exists"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  void createPIXKeyValidationCNPJWrongPatternShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CNPJ.getValue())
        .keyValue("89370199000151")
        .bankBranchNumber(13112)
        .accountNumber(12333L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.JURIDICA.getValue())
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("The entered CNPJ is not valid"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  void createPIXKeyValidationEmailWrongPatternShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.EMAIL.getValue())
        .keyValue("testteste.com.br")
        .bankBranchNumber(13112)
        .accountNumber(12333L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.JURIDICA.getValue())
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("type key EMAIL does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }


  @Test
  void createPIXKeyValidationWrongTypeKeyWrongPatternShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("aaasd")
        .keyValue("312333")
        .bankBranchNumber(11112)
        .accountNumber(1231113L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.FISICA.getValue())
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("typeKey is not supported"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  void createPIXKeyValidationShouldSucess() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("17055433088")
        .bankBranchNumber(1236)
        .accountNumber(112311L)
        .accountHolderFirstName("John")
        .accountHolderLastName("Sheppard")
        .typeEntity(TypeEntity.FISICA.getValue())
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .build();

    underTest.validate(pixKey);

  }


}
