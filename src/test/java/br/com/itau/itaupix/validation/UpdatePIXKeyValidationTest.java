package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeAccount;
import br.com.itau.itaupix.model.TypeEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UpdatePIXKeyValidationTest {

  private UpdatePIXKeyValidation underTest;

  @BeforeEach
  void setUp() {
    underTest = new UpdatePIXKeyValidation();
  }

  @Test
  public void updateValidationWrongTypeKeyShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount("ddasd")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("typeAccount is not supported"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void updateValidationNullTypeAccountShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount(null)
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("typeAccount is not supported"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void updateValidationNullTypeEntityShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .typeEntity(null)
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("typeEntity is not supported"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void updateValidationWrongTypeEntityShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .typeEntity("3123")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("typeEntity is not supported"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }


  @Test
  public void updateValidationShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .typeEntity(TypeEntity.FISICA.getValue())
        .build();

    underTest.validate(pixKey);

  }

  @Test
  public void updateValidationDifferentCombinationShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .typeAccount(TypeAccount.POUPANCA.getValue())
        .typeEntity(TypeEntity.JURIDICA.getValue())
        .build();

    underTest.validate(pixKey);

  }
}
