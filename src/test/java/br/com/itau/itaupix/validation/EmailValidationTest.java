package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmailValidationTest {

  private EmailValidation underTest;

  @BeforeEach
  void setUp() {
    underTest = new EmailValidation();
  }

  @Test
  public void emailValidationWrongFormatShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("email")
        .keyValue("testeteste.com.br")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("type key EMAIL does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void emailValidationNumberCharactersExceededShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("email")
        .keyValue("aasdasd1123132asdasdasdasd13123123312@1321321323212313211323211233123122131111")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("type key EMAIL does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void emailValidationShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("email")
        .keyValue("test@test.com")
        .build();

      underTest.validate(pixKey);
  }

}
