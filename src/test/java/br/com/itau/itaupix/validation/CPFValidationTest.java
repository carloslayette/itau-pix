package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CPFValidationTest {

  private CPFValidation underTest;

  @BeforeEach
  void setUp() {
    underTest = new CPFValidation();
  }

  @Test
  public void cpfValidationLessDigitsShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey("cpf")
        .keyValue("8544938302")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("type key CPF does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
  }

  @Test
  public void cpfValidationMoreDigitsShouldFail() {

    PIXKey pixKey =  PIXKey.builder()
        .typeKey("cpf")
        .keyValue("854493830291")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("type key CPF does not match the expected pattern"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
  }
  @Test
  public void cpfValidationNotValidShouldFail() {

    PIXKey pixKey =  PIXKey.builder()
        .typeKey("cpf")
        .keyValue("85441383029")
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));
    assertTrue(exception.getMessage().contains("The entered CPF is not valid"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));
  }

  @Test
  public void cpfValidationShouldSuccess() {

    PIXKey pixKey =  PIXKey.builder()
        .typeKey("cpf")
        .keyValue("85449383029")
        .build();

    underTest.validate(pixKey);
  }
}
