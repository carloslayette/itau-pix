package br.com.itau.itaupix.validation;

import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeKey;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SearchPIXKeyValidationTest {

  private SearchPIXKeyValidation underTest;

  @BeforeEach
  void setUp() {
    underTest = new SearchPIXKeyValidation();
  }

  @Test
  public void searchPIXKeyValidationWrongParametersFormatShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .id(UUID.fromString("025253d7-50e6-489a-ab34-bc001e3f7549"))
        .bankBranchNumber(2111)
        .typeKey(TypeKey.CPF.getValue())
        .accountNumber(12341L)
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("If id field is informed, the others fields mustn't be included"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void searchPIXKeyValidationWrongFormatDifferentCombinationShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .id(UUID.fromString("025253d7-50e6-489a-ab34-bc001e3f7549"))
        .bankBranchNumber(2123111)
        .typeKey(TypeKey.CNPJ.getValue())
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("If id field is informed, the others fields mustn't be included"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void searchPIXKeyValidationWithoutFilterShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .id(null)
        .bankBranchNumber(null)
        .accountNumber(null)
        .typeKey(null)
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("At least one field needs to be informed"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void searchPIXKeyValidationByIdShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .id(UUID.fromString("025253d7-50e6-489a-ab34-bc001e3f7549"))
        .build();

    underTest.validate(pixKey);

  }

  @Test
  public void searchPIXKeyValidationByTypeKeyShouldFail() {

    PIXKey pixKey = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .build();

    BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class, () -> underTest.validate(pixKey));

    assertTrue(exception.getMessage().contains("The filter must include other fields such as bankBranchNumber and accountNumber"));
    assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

  }

  @Test
  public void searchPIXKeyValidationByBankBankNumberAndAccountNumberShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .bankBranchNumber(3212)
        .accountNumber(321456L)
        .build();

    underTest.validate(pixKey);

  }

  @Test
  public void searchPIXKeyValidationByAllParametersExceptIdShouldSuccess() {

    PIXKey pixKey = PIXKey.builder()
        .accountNumber(321456L)
        .typeKey(TypeKey.CNPJ.getValue())
        .bankBranchNumber(1231)
        .accountNumber(45654L)
        .build();

    underTest.validate(pixKey);

  }
}
