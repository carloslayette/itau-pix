package br.com.itau.itaupix.service;

import br.com.itau.itaupix.dto.*;
import br.com.itau.itaupix.exception.BusinessValidationFailedException;
import br.com.itau.itaupix.exception.PIXKeyNotFoundException;
import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeAccount;
import br.com.itau.itaupix.model.TypeEntity;
import br.com.itau.itaupix.model.TypeKey;
import br.com.itau.itaupix.repository.PIXKeyRepository;
import br.com.itau.itaupix.validation.BusinessValidationFactory;
import br.com.itau.itaupix.validation.CreatePIXKeyValidation;
import br.com.itau.itaupix.validation.SearchPIXKeyValidation;
import br.com.itau.itaupix.validation.UpdatePIXKeyValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PIXKeyServiceTest {

    private IPIXKeyService underTest;

    @Mock
    private PIXKeyRepository pixKeyRepository;

    private UUID defaultUuid = UUID.fromString("c9a70cf6-d772-48c3-887a-5ed31d7acfc4");

    private MockedStatic<UUID> mockedUuid;

    @BeforeEach
    void setUp() {
        underTest = new PIXKeyService(pixKeyRepository,
                new CreatePIXKeyValidation(pixKeyRepository, new BusinessValidationFactory()),
                new UpdatePIXKeyValidation(), new SearchPIXKeyValidation());
    }

    @Test
    void pixKeyServiceAddShouldSuccess() {

        Clock clock = Clock.systemDefaultZone();

        CreatePIXKeyRequest createPIXKeyRequest = CreatePIXKeyRequest.builder()
                .typeKey(TypeKey.EMAIL.getValue())
                .keyValue("test@test.com.br")
                .bankBranchNumber(1234)
                .accountNumber(12345L)
                .accountHolderFirstName("John Leonard")
                .accountHolderLastName("")
                .typeEntity(TypeEntity.JURIDICA.getValue())
                .typeAccount(TypeAccount.POUPANCA.getValue())
                .build();

        try (MockedStatic<UUID> mockedUuid = Mockito.mockStatic(UUID.class)) {
            mockedUuid.when(UUID::randomUUID).thenReturn(defaultUuid);

            CreatePIXKeyResponse expectedResponse = CreatePIXKeyResponse.builder()
                    .id(defaultUuid)
                    .build();

            PIXKey expected = PIXKey.builder()
                    .id(UUID.randomUUID())
                    .typeKey(TypeKey.EMAIL.getValue())
                    .keyValue("test@test.com.br")
                    .bankBranchNumber(1234)
                    .accountNumber(12345L)
                    .accountHolderFirstName("John Leonard")
                    .accountHolderLastName("")
                    .typeEntity(TypeEntity.JURIDICA.getValue())
                    .typeAccount(TypeAccount.POUPANCA.getValue())
                    .creationDate(LocalDateTime.now(clock))
                    .build();

            ArgumentCaptor<PIXKey> argumentCaptor = ArgumentCaptor.forClass(PIXKey.class);

            when(pixKeyRepository.save(argumentCaptor.capture())).thenReturn(expected);

            CreatePIXKeyResponse response = underTest.add(createPIXKeyRequest);
            verify(pixKeyRepository, times(1)).save(expected);

        }

    }

    @Test
    void pixKeyServiceUpdatePIXNotFoundShouldFailed() {

        UpdatePIXKeyRequest updatePIXKeyRequest = UpdatePIXKeyRequest.builder()
                .id(UUID.randomUUID())
                .bankBranchNumber(3212)
                .accountNumber(111111L)
                .accountHolderFirstName("Geralt")
                .accountHolderLastName("Rivia")
                .typeEntity(TypeEntity.FISICA.getValue())
                .typeAccount(TypeAccount.CORRENTE.getValue())
                .build();

        ArgumentCaptor<UUID> argumentCaptor = ArgumentCaptor.forClass(UUID.class);

        when(pixKeyRepository.findById(argumentCaptor.capture())).thenReturn(Optional.empty());

        BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class,
                () -> underTest.update(updatePIXKeyRequest));
        assertTrue(exception.getErrorModel().getError().contains("PIXKey with id " + updatePIXKeyRequest.getId() + " not found"));
        assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

    }

    @Test
    void pixKeyServiceUpdateShouldSuccess() {

        Clock clock = Clock.systemDefaultZone();

        try (MockedStatic<UUID> mockedUuid = Mockito.mockStatic(UUID.class)) {
            mockedUuid.when(UUID::randomUUID).thenReturn(defaultUuid);

            UpdatePIXKeyResponse expectedResponse = UpdatePIXKeyResponse.builder()
                    .id(defaultUuid)
                    .typeKey(TypeKey.CNPJ.getValue())
                    .keyValue("14922739000165")
                    .bankBranchNumber(9999)
                    .accountNumber(3214565L)
                    .accountHolderFirstName("Silvio Santos")
                    .accountHolderLastName("Abravanel")
                    .typeEntity(TypeEntity.FISICA.getValue())
                    .typeAccount(TypeAccount.CORRENTE.getValue())
                    .creationDate(LocalDateTime.now(clock))
                    .build();

            UpdatePIXKeyRequest updatePIXKeyRequest = UpdatePIXKeyRequest.builder()
                    .id(UUID.randomUUID())
                    .bankBranchNumber(3212)
                    .accountNumber(111111L)
                    .accountHolderFirstName("Geralt")
                    .accountHolderLastName("Rivia")
                    .typeEntity(TypeEntity.FISICA.getValue())
                    .typeAccount(TypeAccount.CORRENTE.getValue())
                    .build();

            PIXKey expected = PIXKey.builder()
                    .id(defaultUuid)
                    .typeKey(TypeKey.CNPJ.getValue())
                    .keyValue("14922739000165")
                    .bankBranchNumber(9999)
                    .accountNumber(3214565L)
                    .accountHolderFirstName("Silvio Santos")
                    .accountHolderLastName("Abravanel")
                    .typeEntity(TypeEntity.FISICA.getValue())
                    .typeAccount(TypeAccount.CORRENTE.getValue())
                    .creationDate(LocalDateTime.now(clock))
                    .build();

            ArgumentCaptor<PIXKey> argumentCaptor = ArgumentCaptor.forClass(PIXKey.class);

            ArgumentCaptor<PIXKey> argumentCaptorSave = ArgumentCaptor.forClass(PIXKey.class);

            when(pixKeyRepository.save(argumentCaptor.capture())).thenReturn(expected);
            when(pixKeyRepository.findById(UUID.randomUUID())).thenReturn(Optional.of(expected));

            UpdatePIXKeyResponse actualResponse = underTest.update(updatePIXKeyRequest);

            verify(pixKeyRepository, times(1)).save(argumentCaptorSave.capture());
            assertEquals(expectedResponse, actualResponse);

        }
    }

    @Test
    void pixKeyServiceSearchWithoutAnyParametersShouldFail() {

        SearchPIXKeyRequest searchPIXKeyRequest = SearchPIXKeyRequest.builder()
                .build();

        BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class,
                () -> underTest.search(searchPIXKeyRequest));
        assertTrue(exception.getErrorModel().getError().contains("At least one field needs to be informed"));
        assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

    }

    @Test
    void pixKeyServiceSearchByIdAndOthersParametersShouldFail() {

        SearchPIXKeyRequest searchPIXKeyRequest = SearchPIXKeyRequest.builder()
                .id(UUID.randomUUID().toString())
                .accountNumber(32655L)
                .typeKey(TypeKey.CPF.getValue())
                .bankBranchNumber(23546)
                .build();

        BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class,
                () -> underTest.search(searchPIXKeyRequest));
        assertTrue(exception.getErrorModel().getError().contains("If id field is informed, the others fields mustn't be included"));
        assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

    }

    @Test
    void pixKeyServiceSearchByIdNotFoundShouldFail() {

        SearchPIXKeyRequest searchPIXKeyRequest = SearchPIXKeyRequest.builder()
                .id(UUID.randomUUID().toString())
                .build();

        ArgumentCaptor<Example<PIXKey>> argumentCaptor = ArgumentCaptor.forClass(Example.class);
        when(pixKeyRepository.findAll(argumentCaptor.capture())).thenReturn(new ArrayList<>());

        PIXKeyNotFoundException exception = assertThrows(PIXKeyNotFoundException.class,
                () -> underTest.search(searchPIXKeyRequest));
        assertTrue(exception.getMessage().contains("The filter has returned no results"));

        verify(pixKeyRepository, times(1)).findAll(argumentCaptor.capture());

    }

    @Test
    void pixKeyServiceSearchByIdShouldSuccess() {

        SearchPIXKeyRequest searchPIXKeyRequest = SearchPIXKeyRequest.builder()
                .id(UUID.randomUUID().toString())
                .build();

        PIXKey pixKeyExpected = PIXKey.builder()
                .id(defaultUuid)
                .typeKey(TypeKey.EMAIL.getValue())
                .keyValue("test123@test321.com")
                .bankBranchNumber(9989)
                .accountNumber(124541L)
                .accountHolderFirstName("Jorge Alves")
                .typeEntity(TypeEntity.JURIDICA.getValue())
                .typeAccount(TypeAccount.POUPANCA.getValue())
                .creationDate(LocalDateTime.now())
                .build();

        ArgumentCaptor<Example<PIXKey>> argumentCaptor = ArgumentCaptor.forClass(Example.class);
        when(pixKeyRepository.findAll(argumentCaptor.capture())).thenReturn(Arrays.asList(pixKeyExpected));
        List<SearchPIXKeyResponse> expectedListResponse = underTest.search(searchPIXKeyRequest);

        verify(pixKeyRepository, times(1)).findAll(argumentCaptor.capture());
        assertEquals(expectedListResponse.size(), 1);

    }

    @Test
    void pixKeyServiceSearchByOnlyAccountNumberShouldFail() {

        SearchPIXKeyRequest searchPIXKeyRequest = SearchPIXKeyRequest.builder()
                .accountNumber(321655L)
                .build();

        BusinessValidationFailedException exception = assertThrows(BusinessValidationFailedException.class,
                () -> underTest.search(searchPIXKeyRequest));
        assertTrue(exception.getErrorModel().getError().contains("If bankBranchNumber is informed, accountNumber needs to be informed as well and vice versa"));
        assertTrue(exception.getErrorModel().getPath().contains("/itau-pix/api/v1/pix-key"));

    }

}
