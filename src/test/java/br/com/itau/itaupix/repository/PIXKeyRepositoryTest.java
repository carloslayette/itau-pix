package br.com.itau.itaupix.repository;

import br.com.itau.itaupix.model.PIXKey;
import br.com.itau.itaupix.model.TypeAccount;
import br.com.itau.itaupix.model.TypeEntity;
import br.com.itau.itaupix.model.TypeKey;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class PIXKeyRepositoryTest {

  @Autowired
  private PIXKeyRepository underTest;

  @Test
  void existsPIXKeyByTypeKey() {
    LocalDateTime now = LocalDateTime.now();
    //given
    PIXKey pixKey = PIXKey.builder()
        .typeKey("CPF")
        .keyValue("63903973050")
        .accountNumber(12345678L)
        .bankBranchNumber(1234)
        .accountHolderFirstName("BRANDON LEONARD")
        .typeAccount("corrente")
        .typeEntity("P")
        .creationDate(now)
        .build();

    underTest.save(pixKey);
    //when

    PIXKey expected = underTest.existsPIXKeyByKeyValue(pixKey.getKeyValue());
    //then

    assertThat(pixKey).isEqualTo(expected);

  }

  @Test
  void notExistsPIXKeyByTypeKey() {
    LocalDateTime now = LocalDateTime.now();
    //given
    PIXKey pixKey = PIXKey.builder()
        .typeKey("CPF")
        .keyValue("63903973050")
        .accountNumber(12345678L)
        .bankBranchNumber(1234)
        .accountHolderFirstName("BRANDON LEONARD")
        .typeAccount("corrente")
        .typeEntity("P")
        .creationDate(now)
        .build();

    underTest.save(pixKey);
    //when

    PIXKey actual = null;

    PIXKey expected = underTest.existsPIXKeyByKeyValue("63202923251");
    //then
    assertThat(actual).isEqualTo(expected);

  }

  @Test
  void countByBankBranchNumberAndAccountNumberAndTypeEntity() {

    Long accountNumber = 12345678L;
    Integer bankBranchNumber = 1234;

    PIXKey pixKey1 = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("63903973050")
        .accountNumber(accountNumber)
        .bankBranchNumber(bankBranchNumber)
        .accountHolderFirstName("BRANDON LEONARD")
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .typeEntity(TypeEntity.FISICA.getValue())
        .creationDate(LocalDateTime.now())
        .build();

    PIXKey pixKey2 = PIXKey.builder()
        .typeKey(TypeKey.CPF.getValue())
        .keyValue("64920735000116")
        .accountNumber(accountNumber)
        .bankBranchNumber(bankBranchNumber)
        .accountHolderFirstName("BRANDON LEONARD")
        .typeAccount(TypeAccount.CORRENTE.getValue())
        .typeEntity(TypeEntity.FISICA.getValue())
        .creationDate(LocalDateTime.now())
        .build();

    underTest.save(pixKey1);
    underTest.save(pixKey2);

    Integer count = underTest.countByAccountAndTypeEntity(bankBranchNumber, accountNumber, TypeEntity.FISICA.getValue());

    //then
    assertThat(count).isEqualTo(2);

  }
}
