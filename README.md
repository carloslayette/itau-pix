# Case Itau Pix

Insert, update and search Pix Keys

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
[JDK 17](#https://www.oracle.com/java/technologies/downloads/#java17)
[Maven 3](#https://maven.apache.org/download.cgi)
```

### Run application locally

```
mvn spring-boot:run
```

## Running the tests

```
mvn test
```

## Run the application as a docker image
First we need to generate the jar file:
```
mvn clean package
```
On the project root, run this command to build docker image:
```
docker build -f Dockerfile .
```
And then, from docker folder, run this command
```
docker-compose up -d
```